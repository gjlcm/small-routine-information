//app.js
import md5 from 'utils/md5.js';

App({
    data: {
        domain: "https://gz.haojuhui.cn/"
        //domain: "https://m.haojuhui.cn/"
    },
    onLaunch: function () {
        // 展示本地存储能力
        var logs = wx.getStorageSync('logs') || []
        logs.unshift(Date.now())
        wx.setStorageSync('logs', logs)

        var _this = this;
        // 登录
        wx.login({
            success: function (res) {
                // 发送 res.code 到后台换取 openId, sessionKey, unionId
                _this.postData("wx/checkLogin", {code: res.code}, function (data) {
                    wx.setStorage({key: "sessionId", data: data.data});
                })
            }
        })
        // 获取用户信息
        wx.getSetting({
            success: function (res) {
                // if (res.authSetting['scope.userInfo']) {
                // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
                wx.getUserInfo({
                    success: function (res) {
                        // 可以将 res 发送给后台解码出 unionId
                        _this.globalData.userInfo = res.rawData

                        // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
                        // 所以此处加入 callback 以防止这种情况
                        if (this.userInfoReadyCallback) {
                            this.userInfoReadyCallback(res)
                        }
                    }
                })
                // }
            }
        })
    },
    globalData: {
        userInfo: null,
        appSecret: "dcd15fecfb734f6687c67b897fea7d95"
    },
    loadData: function (url, data, func) {
        if (data) {
            data.sign = this.makeSign(data);
        }
        wx.request({
            url: this.data.domain + url,
            data: data,
            header: {
                "sessionId": wx.getStorageSync("sessionId")
            },
            success: function (data) {
                if (func) {
                    func(data.data);
                }
            }
        })
    },
    postData: function (url, data, func) {
        if (data) {
            data.sign = this.makeSign(data);
        }
        wx.request({
            url: this.data.domain + url,
            data: data,
            method: "post",
            header: {
                "sessionId": wx.getStorageSync("sessionId"),
                "Content-Type": "application/x-www-form-urlencoded"
            },
            success: function (data) {
                if (func) {
                    func(data.data);
                }
            }
        })
    },
    putData: function (url, data, func) {
        if (data) {
            data.sign = this.makeSign(data);
        }
        wx.request({
            url: this.data.domain + url,
            data: data,
            header: {
                "sessionId": wx.getStorageSync("sessionId")
            },
            method: "put",
            success: function (data) {
                if (func) {
                    func(data.data);
                }
            }
        })
    },
    makeSign: function (obj) {
        console.log(obj)
        if (obj == null || JSON.stringify(obj) == "{}") {
            return "";
        }
        var str = "";
        var secret = this.globalData.appSecret;
        //生成key升序数组
        var arr = Object.keys(obj);
        arr.sort();
        for (var i in arr) {
            str += arr[i] + obj[arr[i]];
        }
        var encrypted = md5(str + secret);
        return encrypted;
    }
})