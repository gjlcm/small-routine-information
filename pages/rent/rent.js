const app = getApp()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        contentListMaxHeight: 200,
        navTypeId: 1,
        regionList: [
            {id: "1", text: "遵义"},
            {id: "2", text: "南北"},
            {id: "3", text: "正安"},
            {id: "4", text: "绥阳"},
            {id: "5", text: "仁怀"},
            {id: "6", text: "桐梓"},
            {id: "7", text: "务川"},
            {id: "8", text: "道真"},
            {id: "9", text: "湄潭"},
            {id: "10", text: "习水"},
            {id: "11", text: "凤冈"},
            {id: "12", text: "赤水"},
            {id: "13", text: "余庆"}
        ]
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.setData({userInfo: JSON.parse(app.globalData.userInfo)});
        var _this = this;
        wx.getSystemInfo({
            success: function (res) {
                var query = wx.createSelectorQuery()
                query.select('.types').boundingClientRect()
                query.selectViewport().scrollOffset()
                var height = 0;
                var windowHeight = res.windowHeight;
                query.exec(function (res) {
                    res[0].top       // #the-id节点的上边界坐标
                    height = res[0].height
                    query = wx.createSelectorQuery()
                    query.select('.nav').boundingClientRect();
                    query.selectViewport().scrollOffset();
                    query.exec(function (res) {
                        console.log(height+res[0].height)
                        _this.setData({contentListMaxHeight: (windowHeight - (height + res[0].height + 4))});
                    });
                })


            }
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    clickNav: function (event) {
        this.setData({navTypeId: event.currentTarget.dataset.typeId})
    },
    openLocation: function (event) {
        wx.openLocation({
            longitude:116.491086,
            latitude:40.012311,
            name:"摩托罗拉大厦"
        });
    },
    makePhoneCall: function (event) {
        wx.makePhoneCall({
            phoneNumber: '13141383186'
        })
    }
})