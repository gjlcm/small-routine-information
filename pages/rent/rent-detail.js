const app = getApp()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        userInfo:{},
        swiper: {
            autoplay: true,
            interval: 3000,
            duration: 1000,
            indicatorDots: true,
            banners: [{
                picId: "1",
                picUrl: "https://tct3.ganjistatic1.com/anjkgj/4b3cc5bfa02e7ba41c8832b4ef1a75c4_640-480c_6-0.jpg"
            }, {
                picId: "2",
                picUrl: "http://pic4.58cdn.com.cn/anjuke_58/bb831e88d73359ae0832df86b58ccac9"
            }, {
                picId: "3",
                picUrl: "http://pic5.58cdn.com.cn/anjuke_58/07a891674338fd0e93821833d638443f"
            }]
        }
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.setData({userInfo: JSON.parse(app.globalData.userInfo)})
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    wxParseImgTap: function (event) {
        var that = this;
        var nowImgUrl = event.target.dataset.src;
        wx.previewImage({
            current: "https://tct3.ganjistatic1.com/anjkgj/4b3cc5bfa02e7ba41c8832b4ef1a75c4_640-480c_6-0.jpg", // 当前显示图片的http链接
            urls: ["https://tct3.ganjistatic1.com/anjkgj/4b3cc5bfa02e7ba41c8832b4ef1a75c4_640-480c_6-0.jpg","http://pic4.58cdn.com.cn/anjuke_58/bb831e88d73359ae0832df86b58ccac9","http://pic5.58cdn.com.cn/anjuke_58/07a891674338fd0e93821833d638443f"]//that.data[tagFrom].imageUrls // 需要预览的图片http链接列表
        })
    },
    openLocation:function (event) {
        wx.openLocation({
            latitude:40.012311,
            longitude:116.491086,
            name:"摩托罗拉大厦"
        });
    },
    makePhoneCall:function (event) {
        wx.makePhoneCall({
            phoneNumber:'13141383186'
        })
    }
})