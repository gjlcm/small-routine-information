const app = getApp()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        regionList: [
            {id: "1", text: "家政服务"},
            {id: "2", text: "商务服务"},
            {id: "3", text: "装修建材"},
            {id: "4", text: "教育培训"},
            {id: "5", text: "婚庆摄影"},
            {id: "6", text: "旅游酒店"},
            {id: "7", text: "餐饮美食"},
            {id: "8", text: "休闲娱乐"}
        ],
        navTypeId: 1,
        userInfo:{},
        contentListMaxHeight:962
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.setData({userInfo: JSON.parse(app.globalData.userInfo)})
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    clickNav: function (event) {
        this.setData({navTypeId:event.currentTarget.dataset.typeId})
    },
    makePhoneCall:function(event){
        wx.makePhoneCall({
            phoneNumber:'13141383186'
        })
    },
    openLocation:function (event) {
        wx.openLocation({
            longitude:116.491086,
            latitude:40.012311,
            name:"摩托罗拉大厦"
        });
    }
})