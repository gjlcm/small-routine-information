//index.js
//获取应用实例
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
      swiper:{
          autoplay:true,
          interval:3000,
          duration:1000,
          indicatorDots:true,
          banners:[{
              picId:"1",
              picUrl:"http://img02.tooopen.com/images/20150928/tooopen_sy_143912755726.jpg"
          },{
              picId:"2",
              picUrl:"http://img06.tooopen.com/images/20160818/tooopen_sy_175866434296.jpg"
          },{
              picId:"3",
              picUrl:"http://img06.tooopen.com/images/20160818/tooopen_sy_175833047715.jpg"
          }]
      },
      nav:[
          {
              id:1,
              navigator:"/pages/rent/rent",
              imgUrl:"/images/icon/icon-rent.png",
              title:"找房子",
              navigatorImageColor:"#fd9d21"
          },{
              id:1,
              navigator:"/pages/recruit/recruit",
              imgUrl:"/images/icon/icon-recruit.png",
              title:"找工作",
              navigatorImageColor:"#ff6767"
          },{
              id:1,
              navigator:"",
              imgUrl:"/images/icon/icon-cate.png",
              title:"美食",
              navigatorImageColor:"#4dc6ee"
          },{
              id:1,
              navigator:"",
              imgUrl:"/images/icon/icon-specialty.png",
              title:"土特产",
              navigatorImageColor:"#fed030"
          },{
              id:1,
              navigator:"/pages/carpool/carpool",
              imgUrl:"/images/icon/icon-carpool.png",
              title:"顺风车",
              navigatorImageColor:"#ff80c2"
          },{
              id:1,
              navigator:"/pages/carpool/carpool",
              imgUrl:"/images/icon/icon-loop.png",
              title:"二手",
              navigatorImageColor:"#8a90fa"
          },{
              id:1,
              navigator:"/pages/carpool/carpool",
              imgUrl:"/images/icon/icon-family.png",
              title:"家政",
              navigatorImageColor:"#fd9d21"
          },{
              id:1,
              navigator:"/pages/joke/joke",
              imgUrl:"/images/icon/icon-joke.png",
              title:"开心一刻",
              navigatorImageColor:"#8a90fa"
          }
      ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})
