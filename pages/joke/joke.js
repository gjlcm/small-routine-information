var app = getApp()

// 定义分页查询
var page = 0;
var pageSize = 5;
// 最大id，最小id
var minId = 0;
var maxId = 0;
var loadFlag = false;
Page({

    /**
     * 页面的初始数据
     */
    data: {
        navTypeId: 0,
        contentListMaxHeight: 0,
        contentList: [],
        contentNewList: [],
        loading: true,
        loadmore: false,
        nomore: false,
        nomoreFirst: false
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.getList(0);
        this.getNewJoke(0);
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        if (this.data.navTypeId == 0) {
            this.getList(1);
        } else {
            this.getNewJoke(1);
        }
        wx.stopPullDownRefresh()
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        if (this.data.navTypeId == 0) {
            this.getList(2)
            this.setData({loadmore: true, nomore: false});
        }
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    /**
     * 点击导航
     * @param event
     */
    clickNav: function (event) {
        this.setData({navTypeId: event.currentTarget.dataset.typeId})
    },
    // type：{0:正常加载,1:加载最新,2:加载更多}
    getList: function (type) {
        var _this = this;
        var _id = 0;
        if (type == 1) {
            _id = maxId;
        }
        if (type == 2) {
            _id = minId;
        }
        if (loadFlag) {
            return;
        }
        loadFlag = true;
        /**
         * 加载数据
         */
        app.loadData("joke/selectByPage", {page: page,pageSize: pageSize,id: _id,queryType: type,latest: 1}, function (data) {
            loadFlag = false;
            if (data) {
                var rows = data.rows;
                if (!rows || rows.length == 0) {
                    if (type == 0 || _this.data.contentList.length == 0) {
                        _this.setData({loading: false, loadmore: false, nomoreFirst: true});
                    } else {
                        _this.setData({loading: false, loadmore: false, nomore: true});
                    }
                    return;
                }
                for (var index = 0; index < rows.length; index++) {
                    var content = rows[index].content;
                    if (content) {
                        var _contents = content.split(/\r\n/g);
                        rows[index].content = _contents;
                    }
                    if (rows[index].imgUrl) {
                        rows[index].imgUrl = app.data.domain + "file/readImage?path=" + rows[index].imgUrl;
                    }
                    if (minId == 0) {
                        minId = rows[index].id;
                    }
                    if (rows[index].id > maxId) {
                        maxId = rows[index].id;
                    }
                    if (rows[index].id < minId) {
                        minId = rows[index].id;
                    }
                }
                if (type == 1) {
                    rows = rows.concat(_this.data.contentList);
                }
                if (type == 2) {
                    rows = _this.data.contentList.concat(rows);
                }
                _this.setData({contentList: rows, loading: false, loadmore: false, nomore: false, nomoreFirst: false});
            }
        })
    },
    getNewJoke: function (type) {
        var _this = this;
        app.loadData("joke/selectByPage", {
            page: page,
            pageSize: pageSize,
            id: 0,
            queryType: type,
            latest: 2
        }, function (data) {
            if (data) {
                var rows = data.rows;
                if (!rows || rows.length == 0) {
                    if (type == 0 || _this.data.contentNewList.length == 0) {

                    } else {

                    }
                    return;
                }
                for (var index = 0; index < rows.length; index++) {
                    var content = rows[index].content;
                    if (content) {
                        var _contents = content.split(/\r\n/g);
                        rows[index].content = _contents;
                    }
                    if (rows[index].imgUrl) {
                        rows[index].imgUrl = app.data.domain + "file/readImage?path=" + rows[index].imgUrl;
                    }
                }
                _this.setData({
                    contentNewList: rows,
                    loading: false,
                    loadmore: false,
                    nomore: false,
                    nomoreFirst: false
                });
            }
        })
    },
    clickThumbUp: function (event) {
        var id = event.currentTarget.dataset.id;
        var _type = event.currentTarget.dataset.typeId;
        var _this = this;
        if (id) {
            app.putData("joke/" + id + "/thumbUp", {}, function (data) {
                if (data.data == 1) {
                    var _contentList = _this.data.contentList;
                    if(_type==2){
                        _contentList  = _this.data.contentNewList;
                    }
                    for (var index = 0; index < _contentList.length; index++) {
                        if (id == _contentList[index].id) {
                            var thumbUp = _contentList[index].thumbUp;
                            _contentList[index].thumbUp = thumbUp + 1;
                            if(_type==1){
                                _this.setData({contentList: _contentList});
                            }else{
                                _this.setData({contentNewList: _contentList});
                            }

                            break;
                        }
                    }
                }
            });
        }

    }
})